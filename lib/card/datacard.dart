class food2 {
  String companyName;
  String stack;
  String shortdescription;
  String image;
  String applicants;
  String numApplicant;
  String longdescription;
  bool status;
  food2({
    required this.companyName,
    required this.image,
    required this.shortdescription,
    required this.applicants,
    required this.stack,
    required this.numApplicant,
    required this.longdescription,
    required this.status,
  });
  static List<food2> generatefoods() {
    return [
      food2(
        companyName: '10am to 12 am',
        image: 'assets/restaurant1.jpg',
        stack: 'Time Slot 10am to 12 am',
        shortdescription:
            'Visit our local KFC at 215 W 11th Street for a fresh batch of our rooms and foods',
        applicants: 'Be in the first 20 applicants',
        numApplicant: '20 applicants',
        longdescription:
            'People join our KFC family for a wide variety of reasons, '
            'but they discover so much more about themselves through our training programs,'
            ' family atmosphere and the pride they have in our brand. ',
        status: false,
      ),
      food2(
        companyName: '1pm to 3Pm',
        image: 'assets/restaurant2.jpg',
        stack: 'Time Slot 1pm to 3Pm',
        shortdescription:
            'In a city like Lahore, where every restaurant is offering local '
            'and traditional foods with foreign touch, we can say that it is'
            ' one of the best Marriage Hall that offers vast range of '
            'traditional quality foods in a very reasonable price',
        applicants: 'Be in the first 10 applicants',
        numApplicant: '10 applicants',
        longdescription:
            'In a city like Lahore, where every restaurant is offering local and traditional foods with '
            'foreign touch, we can say that it is one of the best Marriage Hall '
            'that offers vast range of traditional quality foods in a very reasonable price',
        status: false,
      ),
      food2(
        companyName: '3Pm to 5Pm',
        image: 'assets/restaurant2.jpg',
        stack: 'Time Slot 3Pm to 5Pm',
        shortdescription:
            'BBQ Tonight has multiple branches, not only in Pakistan, but also in other countries. '
            'It is considered as one of the best restaurants with growing reviews and consumers’ '
            'satisfaction not only in Pakistan, but also at outside the country..',
        applicants: 'Be in the first 5 applicants',
        numApplicant: '5 applicants',
        longdescription:
            'BBQ Tonight has multiple branches, not only in Pakistan, but also in other countries. '
            'It is considered as one of the best Marriage Hall with growing reviews and consumers’ '
            'satisfaction not only in Pakistan, but also at outside the country.',
        status: false,
      ),
      food2(
        companyName: '5Pm to 8Pm',
        image: 'assets/restaurant4.jpg',
        stack: 'Time Slot 5Pm to 8Pm',
        shortdescription:
            'Lal Qila restaurant has many branches in the country. This Marriage Hall has iconic '
            'importance and there is no foodie who has not heard about this place.'
            ' The menu has a long list of Chinese, Pakistani,'
            ' continental and traditional Mughal foods.',
        applicants: 'Be in the first 2 applicants',
        numApplicant: '2 applicants',
        longdescription: 'Lal Qila restaurant has many branches in the country.'
            ' This restaurant has iconic importance and there is no foodie who has not '
            ' about this place. The menu has a long list of Chinese, '
            'Pakistani, continental and traditional Mughal foods.',
        status: false,
      ),
    ];
  }
}
