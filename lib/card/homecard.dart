// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:ui';

import 'package:email_password_login/emploee/pages/hotelsRecord.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Admin/Admin.dart';
import '../Request/userrequest/pages/addpage.dart';
import '../payment/payment.dart';
import '../screens/login_screen.dart';
import '../splash.dart';
import 'datacard.dart';
import 'detailcard.dart';

class UserDashboard extends StatefulWidget {
  @override
  _UserDashboardState createState() => _UserDashboardState();
}

class _UserDashboardState extends State<UserDashboard> {
  final _list = food2.generatefoods();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Marriage Hall"),
        backgroundColor: Colors.orange,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text("Marriage Hall"),
              accountEmail: Text("Zafar123@gmail.com"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/use1.jpg'),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/bgg.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            // ListTile(
            //             //   leading: Icon(Icons.admin_panel_settings),
            //             //   title: Text("Admin"),
            //             //   onTap: () {
            //             //     Navigator.pushAndRemoveUntil(
            //             //         context,
            //             //         MaterialPageRoute(
            //             //           builder: (context) => MyAppad(),
            //             //         ),
            //             //         (route) => false);
            //             //   },
            //             // ),
            ListTile(
              leading: Icon(Icons.record_voice_over),
              title: Text("Halls Record"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HotelsRecordAdmin(),
                    ),
                        (route) => false);
              },
            ),
            ListTile(
              leading: Icon(Icons.food_bank),
              title: Text("Token Money"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomeScreenpay(),
                    ),
                        (route) => false);
              },
            ),

            ListTile(
              leading: Icon(Icons.event),
              title: Text("Add Request"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddPage(),
                    ),
                        (route) => false);
              },
            ),


            ListTile(
              leading: Icon(Icons.cancel),
              title: Text("Cancel Event"),
              onTap: () {
                // Navigator.pushAndRemoveUntil(
                //     context,
                //     MaterialPageRoute(
                //       builder: (context) => SplashScreen(),
                //     ),
                //         (route) => false);
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginScreen(),
                    ),
                        (route) => false);
              },
            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: ColumnBody(),
      ),
    );
  }
}

class ColumnBody extends StatefulWidget {
  @override
  State<ColumnBody> createState() => _ColumnBodyState();
}

class _ColumnBodyState extends State<ColumnBody> {
  @override
  Widget build(BuildContext context) {
    final _list = food2.generatefoods();
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 4.0),
            child: SizedBox(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              ),
            ),
          ),


          Container(
            padding: EdgeInsets.only(bottom: 5.0, top: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Recommended Time Slots',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 10.0),
                ),
              ],
            ),
          ),
          ListView.separated(
            padding: EdgeInsets.zero,
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              var job = _list[index];
              if (job.status) {
                index = index + 1;
                return Container();
              } else {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => JobDetails(
                          jobs: job,
                        ),
                      ),
                    );
                  },
                  child: Card(
                    color: Colors.orange,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(15.0),
                      ),
                    ),
                    child: Container(
                      margin: EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(8.0),
                                child: Image.asset(
                                  job.image,
                                  width: 30,
                                  height: 30,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 5.0),
                                child: Text(
                                  job.companyName,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              Spacer(),
                              Container(
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: IconButton(
                                    icon: const Icon(Icons.bookmark),
                                    color: Colors.grey,
                                    iconSize: 15,
                                    onPressed: () {},
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              job.stack,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              job.shortdescription,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                              maxLines: 4,
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.timer_outlined,
                                    color: Colors.white,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 3.0),
                                  child: Text(
                                    'Be in the first 20 applicants',
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }
            },
            separatorBuilder: (context, index) => Divider(),
            itemCount: _list.length,
          ),
        ],
      ),
    );
  }
}


