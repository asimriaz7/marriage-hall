// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
//
// import '../card/homecard.dart';
//
// import 'info.dart';
// import 'info1.dart';
// import 'info2.dart';
// import 'info3.dart';
//
// void main() => runApp(MyApp());
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     SystemChrome.setPreferredOrientations([
//       DeviceOrientation.portraitUp,
//     ]);
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData.light(),
//       home: MyPage(),
//     );
//   }
// }
//
// class MyPage extends StatefulWidget {
//   @override
//   _MyPageState createState() => _MyPageState();
// }
//
// class _MyPageState extends State<MyPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Available Food"),
//         backgroundColor: Colors.orange,
//         leading: IconButton(
//           icon: Icon(Icons.arrow_back, color: Colors.white),
//           onPressed: () {
//             Navigator.pushAndRemoveUntil(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => Homecard(),
//                 ),
//                 (route) => false);
//           },
//         ),
//       ),
//       body: Padding(
//         padding: EdgeInsets.all(5),
//         child: ListView(
//           scrollDirection: Axis.vertical,
//           children: <Widget>[
//             Hero(
//               tag: "Karahi",
//               child: FittedBox(
//                 child: GestureDetector(
//                   onTap: () {
//                     Navigator.push(
//                       context,
//                       MaterialPageRoute(builder: (context) => InfoPage()),
//                     );
//                   },
//                   child: Card(
//                     // color: Colors.red,
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(15.0),
//                     ),
//                     elevation: 5,
//                     child: Row(
//                       children: <Widget>[
//                         itemcake(),
//                         Container(
//                           width: 90,
//                           height: 100,
//                           child: ClipRRect(
//                             borderRadius: BorderRadius.circular(15.0),
//                             child: Image(
//                               fit: BoxFit.cover,
//                               alignment: Alignment.topRight,
//                               image: AssetImage('assets/karahi.jpg'),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//             FittedBox(
//               child: GestureDetector(
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) => Info1Page()),
//                   );
//                 },
//                 child: Card(
//                   // color: Colors.red,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                   ),
//                   elevation: 5,
//                   child: Row(
//                     children: <Widget>[
//                       juiceitem(),
//                       Container(
//                         width: 90,
//                         height: 100,
//                         child: ClipRRect(
//                           borderRadius: BorderRadius.circular(15.0),
//                           child: Image(
//                             fit: BoxFit.cover,
//                             alignment: Alignment.topRight,
//                             image: AssetImage('assets/juice.jpg'),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             FittedBox(
//               child: GestureDetector(
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) => Info2Page()),
//                   );
//                 },
//                 child: Card(
//                   // color: Colors.red,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                   ),
//                   elevation: 5,
//                   child: Row(
//                     children: <Widget>[
//                       pizzaitem(),
//                       Container(
//                         width: 90,
//                         height: 100,
//                         child: ClipRRect(
//                           borderRadius: BorderRadius.circular(15.0),
//                           child: Image(
//                             fit: BoxFit.cover,
//                             alignment: Alignment.topRight,
//                             image: AssetImage('assets/pizza.jpg'),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             FittedBox(
//               child: GestureDetector(
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) => Info3Page()),
//                   );
//                 },
//                 child: Card(
//                   // color: Colors.red,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                   ),
//                   elevation: 5,
//                   child: Row(
//                     children: <Widget>[
//                       eliteitem(),
//                       Container(
//                         width: 90,
//                         height: 100,
//                         child: ClipRRect(
//                           borderRadius: BorderRadius.circular(15.0),
//                           child: Image(
//                             fit: BoxFit.cover,
//                             alignment: Alignment.topRight,
//                             image: AssetImage('assets/burger.jpg'),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget itemcake() {
//     return Container(
//       //width: 150,
//       child: Column(
//         //mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.symmetric(horizontal: 5),
//             child: Text(
//               "Pakistani Cheicken Karahi",
//               style: TextStyle(
//                   fontWeight: FontWeight.bold, fontSize: 15, color: Colors.red),
//             ),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Text(
//             "Our Karahi Is Delicious",
//             style: TextStyle(
//                 fontWeight: FontWeight.normal,
//                 fontSize: 9.5,
//                 color: Colors.grey),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.shopping_cart,
//                 size: 15,
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.lightBlue[100],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Tasty",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.red[100],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Fresh",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: <Widget>[
//               Text(
//                 "Ratings",
//                 style: TextStyle(
//                     fontWeight: FontWeight.normal,
//                     fontSize: 7,
//                     color: Colors.grey),
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget juiceitem() {
//     return Container(
//       //width: 150,
//       child: Column(
//         //mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.symmetric(horizontal: 5),
//             child: Text(
//               "Fresh Mango Juice",
//               style: TextStyle(
//                   fontWeight: FontWeight.bold, fontSize: 15, color: Colors.red),
//             ),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Text(
//             "Enjoy Fresh Juice",
//             style: TextStyle(
//                 fontWeight: FontWeight.normal,
//                 fontSize: 9.5,
//                 color: Colors.grey),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.shopping_cart,
//                 size: 15,
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.lightBlue[100],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Cold",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.red[100],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Fresh",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.yellow[400],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "New",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: <Widget>[
//               Text(
//                 "Ratings",
//                 style: TextStyle(
//                     fontWeight: FontWeight.normal,
//                     fontSize: 7,
//                     color: Colors.grey),
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget pizzaitem() {
//     return Container(
//       //width: 150,
//       child: Column(
//         //mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.symmetric(horizontal: 5),
//             child: Text(
//               "Cheese Pizza Italy ",
//               style: TextStyle(
//                   fontWeight: FontWeight.bold, fontSize: 15, color: Colors.red),
//             ),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Text(
//             "Double cheese Pakistani Style",
//             style: TextStyle(
//                 fontWeight: FontWeight.normal,
//                 fontSize: 9.5,
//                 color: Colors.grey),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.shopping_cart,
//                 size: 15,
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.deepOrange[300],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Spicy",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.yellow[400],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "New",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: <Widget>[
//               Text(
//                 "Ratings",
//                 style: TextStyle(
//                     fontWeight: FontWeight.normal,
//                     fontSize: 7,
//                     color: Colors.grey),
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget eliteitem() {
//     return Container(
//       //width: 150,
//       child: Column(
//         //mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.symmetric(horizontal: 5),
//             child: Text(
//               "Burger",
//               style: TextStyle(
//                   fontWeight: FontWeight.bold, fontSize: 15, color: Colors.red),
//             ),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Text(
//             "Best Burger",
//             style: TextStyle(
//                 fontWeight: FontWeight.normal,
//                 fontSize: 9.5,
//                 color: Colors.grey),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.shopping_cart,
//                 size: 15,
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.deepOrange[300],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Spicy",
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 9.5),
//                 ),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.red,
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "Hot",
//                   style: TextStyle(
//                       fontWeight: FontWeight.bold,
//                       fontSize: 9.5,
//                       color: Colors.white),
//                 ),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                 width: 35,
//                 decoration: BoxDecoration(
//                   color: Colors.yellow[400],
//                   //color: Theme.of(context).accentColor,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 alignment: Alignment.center,
//                 child: Text(
//                   "New",
//                   style: TextStyle(
//                     fontWeight: FontWeight.bold,
//                     fontSize: 9.5,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             children: <Widget>[
//               Text(
//                 "Ratings",
//                 style: TextStyle(
//                     fontWeight: FontWeight.normal,
//                     fontSize: 7,
//                     color: Colors.grey),
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//               Icon(
//                 Icons.star,
//                 size: 10,
//                 color: Colors.orangeAccent,
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }
