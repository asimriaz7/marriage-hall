// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/emploee/pages/hotelsRecord.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';

import '../../Admin/Admin.dart';
import '../../Request/userrequest/service/firebasecrud.dart';
import '../../card/homecard.dart';

class MyBookings extends StatefulWidget {
  const MyBookings({Key? key}) : super(key: key);

  @override
  State<MyBookings> createState() => _MyBookingsState();
}

class _MyBookingsState extends State<MyBookings> {
  final Stream<QuerySnapshot> BookingcollectionReference =
      FirebaseCrud.readBookings();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Booking Request"),
        backgroundColor:Colors.orange,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => MyAppad(),
                ),
                    (route) => false);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: StreamBuilder<QuerySnapshot>(
              stream: BookingcollectionReference,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                } else if (snapshot.connectionState == ConnectionState.active ||
                    snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    print("I am in Error State in conversation Screen");
                    return Text(snapshot.error.toString());
                  } else if (snapshot.hasData) {
                    return ListView.separated(
                      separatorBuilder: (context, index) => Divider(),
                      itemCount: snapshot.data!.docs.length,
                      padding: EdgeInsets.zero,
                      physics: NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        DocumentSnapshot ds = snapshot.data!.docs[index];
                        if (ds['status']) {
                          index = index + 1;
                          return Container();
                        } else {
                          return GestureDetector(
                            onTap: () {},
                            child: Card(
                              color: Colors.orange,
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                              ),
                              child: Container(
                                margin: EdgeInsets.all(15.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(8.0),
                                          child: Image.asset(
                                            ds['image'],
                                            width: 30,
                                            height: 30,
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            ds['companyName'],
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        Container(
                                          height: 30,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                          ),
                                          child: Center(
                                            child: IconButton(
                                              icon: const Icon(Icons.bookmark),
                                              color: Colors.grey,
                                              iconSize: 15,
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8.0),
                                      child: Text(
                                        ds['stack'],
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8.0),
                                      child: Text(
                                        ds['Sdesc'],
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                        maxLines: 4,
                                      ),
                                    ),
                                    Container(
                                      child: Row(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(8.0),
                                            child: Icon(
                                              Icons.timer_outlined,
                                              color: Colors.white,
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 3.0),
                                            child: Text(
                                              ds['numApplicant'],
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                primary: Colors.white,
                                              ),
                                              onPressed: () async {
                                                var response =
                                                    await FirebaseCrud
                                                        .updateBookingStatus(
                                                            companyName: ds[
                                                                'companyName'],
                                                            status: true);
                                                if (response.code != 200) {
                                                  final snackbar = SnackBar(
                                                    content: Text(response
                                                        .message
                                                        .toString()),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackbar);
                                                } else {
                                                  final Email email = Email(
                                                    body:
                                                        'Your Booking Time ${ds['companyName']} has been Confirmed',
                                                    subject:
                                                        'Booking Confirmation',
                                                    recipients: [
                                                      '${ds['email']}'
                                                    ],
                                                    isHTML: false,
                                                  );
                                                  String platformResponse;
                                                  try {
                                                    await FlutterEmailSender
                                                        .send(email);
                                                    platformResponse =
                                                        'success';
                                                  } catch (error) {
                                                    print(
                                                        "I am error  ${error}");
                                                    platformResponse =
                                                        error.toString();
                                                  }

                                                  final snackbar = SnackBar(
                                                    content: Text(
                                                        "Booking Accepted"),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackbar);
                                                }
                                              },
                                              child: Text("Accept")),
                                          SizedBox(
                                            width: 8,
                                          ),
                                          ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                primary: Colors.white,
                                              ),
                                              onPressed: () async {
                                                var response =
                                                    await FirebaseCrud
                                                        .updateBookingStatus(
                                                            companyName: ds[
                                                                'companyName'],
                                                            status: false);
                                                if (response.code != 200) {
                                                  final snackbar = SnackBar(
                                                    content: Text(response
                                                        .message
                                                        .toString()),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackbar);
                                                } else {
                                                  final Email email = Email(
                                                    body:
                                                        'Your Booking Time ${ds['companyName']} has been Rejected',
                                                    subject:
                                                        'Booking Confirmation',
                                                    recipients: [
                                                      '${ds['email']}'
                                                    ],
                                                    isHTML: false,
                                                  );
                                                  String platformResponse;
                                                  try {
                                                    await FlutterEmailSender
                                                        .send(email);
                                                    platformResponse =
                                                        'success';
                                                  } catch (error) {
                                                    print(
                                                        "I am error  ${error}");
                                                    platformResponse =
                                                        error.toString();
                                                  }

                                                  final snackbar = SnackBar(
                                                    content: Text(
                                                        "Booking Rejected"),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackbar);
                                                }
                                              },
                                              child: Text("Reject"))
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }
                      },
                    );
                  } else {
                    return Text("Empty Data");
                  }
                } else {
                  return Text('State: ${snapshot.connectionState}');
                }
              },
            )),
      ),
    );
  }
}
