import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/Request/userrequest/service/firebasecrud.dart';
import 'package:flutter/material.dart';

import '../../card/homecard.dart';

class HotelsRecordAdmin extends StatefulWidget {
  const HotelsRecordAdmin({Key? key}) : super(key: key);

  @override
  State<HotelsRecordAdmin> createState() => _HotelsRecordAdminState();
}

class _HotelsRecordAdminState extends State<HotelsRecordAdmin> {
  final Stream<QuerySnapshot> hotelscollectionReference =
      FirebaseCrud.readBookings();
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: ElevatedButton(
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.black)))),
          child: Text("Booked Slots"),
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => AvailableSlots(),
                ),
                (route) => false);
          },
        ),
        appBar: AppBar(
          title: Text("Available"),
          backgroundColor:Colors.orange,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UserDashboard(),
                  ),
                      (route) => false);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: StreamBuilder<QuerySnapshot>(
                stream: hotelscollectionReference,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (snapshot.connectionState ==
                          ConnectionState.active ||
                      snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      print("I am in Error State in conversation Screen");
                      return Text(snapshot.error.toString());
                    } else if (snapshot.hasData) {
                      return ListView.separated(
                        separatorBuilder: (context, index) => Divider(),
                        itemCount: snapshot.data!.docs.length,
                        padding: EdgeInsets.zero,
                        physics: NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          DocumentSnapshot ds = snapshot.data!.docs[index];
                          if (ds['status']) {
                            index = index + 1;
                            return Container();
                          } else {
                            return GestureDetector(
                              onTap: () {},
                              child: Card(
                                color: Colors.orange,
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(15.0),
                                  ),
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(15.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(8.0),
                                            child: Image.asset(
                                              ds['image'],
                                              width: 30,
                                              height: 30,
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 5.0),
                                            child: Text(
                                              ds['companyName'],
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            height: 30,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: IconButton(
                                                icon:
                                                    const Icon(Icons.bookmark),
                                                color: Colors.grey,
                                                iconSize: 15,
                                                onPressed: () {},
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        child: Text(
                                          ds['stack'],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        child: Text(
                                          ds['Sdesc'],
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                          maxLines: 4,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                        },
                      );
                    } else {
                      return Text("Empty Data");
                    }
                  } else {
                    return Text('State: ${snapshot.connectionState}');
                  }
                },
              )),
        ));
  }
}

class AvailableSlots extends StatefulWidget {
  const AvailableSlots({Key? key}) : super(key: key);

  @override
  State<AvailableSlots> createState() => _AvailableSlotsState();
}

class _AvailableSlotsState extends State<AvailableSlots> {
  final Stream<QuerySnapshot> hotelscollectionReference =
      FirebaseCrud.readBookings();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Booked Slots"),
          backgroundColor:Colors.orange,

          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UserDashboard(),
                  ),
                      (route) => false);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: StreamBuilder<QuerySnapshot>(
                stream: hotelscollectionReference,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (snapshot.connectionState ==
                          ConnectionState.active ||
                      snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      print("I am in Error State in conversation Screen");
                      return Text(snapshot.error.toString());
                    } else if (snapshot.hasData) {
                      return ListView.separated(
                        separatorBuilder: (context, index) => Divider(),
                        itemCount: snapshot.data!.docs.length,
                        padding: EdgeInsets.zero,
                        physics: NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          DocumentSnapshot ds = snapshot.data!.docs[index];
                          if (!ds['status']) {
                            index = index + 1;
                            return Container();
                          } else {
                            return GestureDetector(
                              onTap: () {},
                              child: Card(
                                color: Colors.orange,
                                elevation: 5,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(15.0),
                                  ),
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(15.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.all(8.0),
                                            child: Image.asset(
                                              ds['image'],
                                              width: 30,
                                              height: 30,
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 5.0),
                                            child: Text(
                                              ds['companyName'],
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            height: 30,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Center(
                                              child: IconButton(
                                                icon:
                                                    const Icon(Icons.bookmark),
                                                color: Colors.grey,
                                                iconSize: 15,
                                                onPressed: () {},
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        child: Text(
                                          ds['stack'],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(8.0),
                                        child: Text(
                                          ds['Sdesc'],
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                          maxLines: 4,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                        },
                      );
                    } else {
                      return Text("Empty Data");
                    }
                  } else {
                    return Text('State: ${snapshot.connectionState}');
                  }
                },
              )),
        ));
  }
}
