import 'package:email_password_login/screens/login_screen.dart';
import 'package:email_password_login/splash.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';

import 'card/homecard.dart';


Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();
  Stripe.publishableKey="pk_test_51LXOixDkEgh0dVhUdknEo50xVOWLJL4i9X6A7z03lHgBJavR96BrjD0w2X9zQ1lOjHkffGQ8BPauqa4iuJPGHh2l001cq1Yutg";
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Marriage Hall',
      theme: ThemeData(
        primarySwatch: Colors.orange,

      ),
      home: SplashScreen(),
    );
  }
}
