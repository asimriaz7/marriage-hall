import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';

import '../../../Admin/Admin.dart';
import '../../../card/homecard.dart';
import '../models/emploee.dart';
import '../service/firebasecrud.dart';
import 'addpage.dart';
import 'editpage.dart';


class Listrequest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Listrequest();
  }
}

class _Listrequest extends State<Listrequest> {
  final Stream<QuerySnapshot> collectionReference = FirebaseCrud.readRequest();
  //FirebaseFirestore.instance.collection('Employee').snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("List Request"),
        backgroundColor:Colors.orange,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => MyAppad(),
                ),
                    (route) => false);
          },
        ),
      ),
      body: StreamBuilder(
        stream: collectionReference,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ListView(
                children: snapshot.data!.docs.map((e) {
                  return Card(
                      child: Column(children: [
                        ListTile(
                          title: Text(e["employee_name"]),
                          subtitle: Container(
                            child: (Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Date: " + e['quali'],
                                    style: const TextStyle(fontSize: 14)),
                                Text("Venu: " + e['position'],
                                    style: const TextStyle(fontSize: 14)),
                                Text("Menu: " + e['contact_no'],
                                    style: const TextStyle(fontSize: 12)),
                              ],
                            )),
                          ),
                        ),
                        ButtonBar(
                          alignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: const EdgeInsets.all(5.0),
                                primary: const Color.fromARGB(255, 143, 133, 226),
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              child: const Text('Edit'),
                              onPressed: () {
                                Navigator.pushAndRemoveUntil<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) => EditPage(
                                      Rrequest: Request(
                                          uid: e.id,
                                          employeename: e["employee_name"],
                                          position: e["position"],
                                          contactno: e["contact_no"],
                                        qualification: e["quali"]),
                                    ),
                                  ),
                                      (route) =>
                                  false, //if you want to disable back feature set to false
                                );
                              },
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                padding: const EdgeInsets.all(5.0),
                                primary: const Color.fromARGB(255, 143, 133, 226),
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              child: const Text('Cancel Request'),
                              onPressed: () async {
                                var response =
                                await FirebaseCrud.deleteRequest(docId: e.id);
                                if (response.code != 200) {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          content:
                                          Text(response.message.toString()),
                                        );
                                      });
                                }
                              },
                            ),
                          ],
                        ),
                      ]));
                }).toList(),
              ),
            );
          }

          return Container();
        },
      ),
    );
  }
}