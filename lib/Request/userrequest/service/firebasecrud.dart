import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/response.dart';

final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _Collection = _firestore.collection('Request');

class FirebaseCrud {
  static Future<Response> addEmployee({
    required String name,
    required String position,
    required String contactno,
    required String quali,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer = _Collection.doc();

    Map<String, dynamic> data = <String, dynamic>{
      "employee_name": name,
      "position": position,
      "contact_no": contactno,
      "quali": quali
    };

    var result = await documentReferencer.set(data).whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully added to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

  static Future<Response> updateRequest({
    required String name,
    required String position,
    required String contactno,
    required String quali,
    required String docId,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer = _Collection.doc(docId);

    Map<String, dynamic> data = <String, dynamic>{
      "employee_name": name,
      "position": position,
      "contact_no": contactno,
      "quali": quali,
    };

    await documentReferencer.update(data).whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully updated Employee";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

  static Stream<QuerySnapshot> readRequest() {
    CollectionReference notesItemCollection = _Collection;

    return notesItemCollection.snapshots();
  }

  static Stream<QuerySnapshot> readHotels() {
    CollectionReference notesItemCollection = _firestore.collection('Hotels');

    return notesItemCollection.snapshots();
  }

  static Stream<QuerySnapshot> readBookings() {
    CollectionReference notesItemCollection = _firestore.collection('Bookings');

    return notesItemCollection.snapshots();
  }

  static Future<Response> deleteRequest({
    required String docId,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer = _Collection.doc(docId);

    await documentReferencer.delete().whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully Deleted Employee";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

  static Future<Response> addBookings(
      {required String companyName,
      required String image,
      required String numApplicant,
      required String stack,
      required String Sdesc,
      required String Ldesc,
      required bool status,
      required String email}) async {
    Response response = Response();
    DocumentReference documentReferencer = _Collection.doc();

    Map<String, dynamic> data = <String, dynamic>{
      "companyName": companyName,
      "image": image,
      "numApplicant": numApplicant,
      "stack": stack,
      "Sdesc": Sdesc,
      "Ldesc": Ldesc,
      "status": status,
      "email": email
    };

    var result = await FirebaseFirestore.instance
        .collection("Bookings")
        .doc(data['companyName'])
        .set(data)
        .whenComplete(() {
      response.code = 200;
      response.message = "Booking Sucessfully added to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

  static Future<Response> updateBookingStatus({
    required String companyName,
    required bool status,
  }) async {
    Response response = Response();

    var result = await FirebaseFirestore.instance
        .collection("Bookings")
        .doc(companyName)
        .update({'status': status}).whenComplete(() {
      response.code = 200;
      response.message = "Booking Sucessfully added to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }
}
