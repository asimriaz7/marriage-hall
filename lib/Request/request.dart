import 'package:flutter/material.dart';

import '../card/homecard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Event Book',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home:MyFormPage(),
    );
  }
}
enum item { Buffet,IceCream,Drinks,Rice,karahi,Biryani,Breakfast,Lunch }
class MyFormPage extends StatefulWidget{
  @override
  _MyFormPageState createState() =>_MyFormPageState();
}
class _MyFormPageState extends State<MyFormPage>{
  final global_form_key=GlobalKey<FormState>();
  var name="";
  var age="";
  item gender=item.Buffet;
  bool checkLower=false;
  bool checkkarahi=false;
  bool checkbiryani=false;
  bool checkice=false;
  bool checkLunch=false;
  bool checkbreakfast=false;
  bool checkdinner=false;
  bool checkDrink=false;
  bool checkUpper=false;
  bool checktobah=false;

  var seat_available=[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Marriage Hall"),
          backgroundColor: Color(0xff008080),

          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => UserDashboard(),
                  ),
                      (route) => false);
            },
          ),
        ),

        body:Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
              key: global_form_key,
              child: Column(
                children:<Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Event Type',
                    ),
                    validator: (value){
                      name=value!;
                      if(value.isEmpty){
                        return 'please enter';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Venue',
                    ),
                    validator: (value){
                      name=value!;
                      if(value.isEmpty){
                        return 'please enter ';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Venue Type',
                    ),
                    validator: (value){
                      name=value!;
                      if(value.isEmpty){
                        return 'please enter ';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Venue Type',
                    ),
                    validator: (value){
                      name=value!;
                      if(value.isEmpty){
                        return 'please enter ';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    keyboardType: TextInputType.visiblePassword,
                    decoration: InputDecoration(
                      hintText: 'Event Date',
                    ),
                    validator: (value){
                      age=value!;
                      if(value.isEmpty){
                        return 'please enter correct ';
                      }
                      return null;
                    },
                  ),
                  // ListTile(
                  //   title: Text("male"),
                  //
                  //   leading: Radio(
                  //     value: item.Buffet,
                  //     groupValue: gender,
                  //     onChanged: (item? value) {
                  //       // setState(() {
                  //       //   gender=value!;
                  //       // });
                  //     },
                  //
                  //   ),
                  // )
                  // ,ListTile(
                  //   title: Text("female"),
                  //
                  //   leading: Radio(
                  //     value: item.IceCream,
                  //     groupValue: gender,
                  //     onChanged: (item? value) {
                  //       // setState(() {
                  //       //   gender=value!;
                  //       // });
                  //     },
                  //
                  //   ),
                  // ),
                  CheckboxListTile(
                    title: Text("Buffet"),
                    checkColor: Colors.blueAccent,
                    activeColor: Colors.orange,
                    onChanged: (bool? value) {
                      setState(() {
                        this.checkLower=value!;

                        if(value){
                          seat_available.add('Buffet');}
                        if(!value){
                          seat_available.remove('Buffet');
                        }

                      });

                    },
                    value:this.checkLower,
                  ),
                  CheckboxListTile(
                    title: Text("KARAHI"),
                    checkColor: Colors.blueAccent,
                    activeColor: Colors.orange,
                    onChanged: (bool? value) {
                      setState(() {
                        this.checkkarahi=value!;

                        if(value){
                          seat_available.add('KARAHI');}
                        if(!value){
                          seat_available.remove('KARAHI');
                        }

                      });

                    },
                    value:this.checkkarahi,
                  ),
                  CheckboxListTile(
                    title: Text("Biryani"),
                    checkColor: Colors.blueAccent,
                    activeColor: Colors.orange,
                    onChanged: (bool? value) {
                      setState(() {
                        this.checkbiryani=value!;

                        if(value){
                          seat_available.add('Biryani');}
                        if(!value){
                          seat_available.remove('Biryani');
                        }

                      });

                    },
                    value:this.checkbiryani,
                  ),
                  CheckboxListTile(
                    title: Text("BreakFast"),
                    checkColor: Colors.blueAccent,
                    activeColor: Colors.orange,
                    onChanged: (bool? value) {
                      setState(() {
                        this.checkdinner=value!;

                        if(value){
                          seat_available.add('BreakFast');}
                        if(!value){
                          seat_available.remove('BreakFast');
                        }

                      });

                    },
                    value:this.checkdinner,
                  ),
                  CheckboxListTile(
                    title: Text("Dinner"),
                    checkColor: Colors.blueAccent,
                    activeColor: Colors.orange,
                    onChanged: (bool? value) {
                      setState(() {
                        this.checkDrink=value!;

                        if(value){
                          seat_available.add('Dinner');}
                        if(!value){
                          seat_available.remove('Dinner');
                        }

                      });

                    },
                    value:this.checkDrink,
                  ),


                  CheckboxListTile(
                    title: Text("IceCream"),
                    checkColor: Colors.blueAccent,
                    activeColor: Colors.orange,

                    onChanged: (bool? value) {
                      setState(() {
                        this.checktobah=value!;
                        if(value){
                          seat_available.add('IceCream');}
                        if(!value){
                          seat_available.remove('IceCream');}
                      });

                    },
                    value:this.checktobah),

            RaisedButton(
              color:  Color(0xff008080),
             // background
              textColor: Colors.white, // foreground
              onPressed: () { },
              child: Text('Send Request'),
            )


                ],
              )
          ),
        )
    );
  }

}