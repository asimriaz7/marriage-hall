List<Map<String, dynamic>> selectionData = [
  {
    "image": "assets/restaurant1.jpg",
    "title": "Macdonalds",
    "star_count": 4,
    "reviews": 112,
    "address": "Multan",
    "full_address": "41 Rue De Penthievre 75008 Multan",
    "description":
        "Macdonalds is a culinary brand established by chef David Chang in 2004 with the opening of Momofuku Noodle bar.",
  },
  {
    "image": "assets/restaurant2.jpg",
    "title": "KFC",
    "star_count": 3,
    "reviews": 72,
    "address": "KFC 15e",
    "full_address": "48Penthievre 65008 Paris",
    "description":
        "KFC is a culinary brand established by chef John Smith in 2015 with the opening of Gaia Club Paradise Burger bar.",
  },
  {
    "image": "assets/restaurant3.jpg",
    "title": "Khan",
    "star_count": 4,
    "reviews": 112,
    "address": "Multan",
    "full_address": "41 Rue De Penthievre 75008 Multan",
    "description":
    "Macdonalds is a culinary brand established by chef David Chang in 2004 with the opening of Momofuku Noodle bar.",
  },
  {
    "image": "assets/restaurant4.jpg",
    "title": "Brand Embercy",
    "star_count": 3,
    "reviews": 72,
    "address": "KFC 15e",
    "full_address": "48Penthievre 65008 Paris",
    "description":
    "KFC is a culinary brand established by chef John Smith in 2015 with the opening of Gaia Club Paradise Burger bar.",
  },
];
List<Map<String, dynamic>> recommended = [
  {
    "image": "assets/restaurant3.jpg",
    "title": "Pate & Co",
    "star_count": 5,
    "reviews": 45,
    "address": "Paris 14e",
    "full_address": "41 Rue De Penthievre 75008 Paris",
    "description":
        "Pate & Co is a culinary brand established by chef Chang in 2008 with the opening of Pate & Co Cake bar.",
  },
  {
    "image": "assets/restaurant4.jpg",
    "title": "Boxcar Social",
    "star_count": 4,
    "reviews": 21,
    "address": "Paris 18e",
    "full_address": "48Penthievre 65008 Paris",
    "description":
        "Boxcar Social is a culinary brand established by chef Smith in 2013 with the opening of Boxcar Social BBQ bar.",
  },
];
List<Map<String, dynamic>> menu = [
  {
    "image": "assets/restaurant3.jpg",
    "title": "Pate & Co",
    "star_count": 5,
    "reviews": 45,
    "price": 14,
    "address": "Paris 14e",
    "full_address": "41 Rue De Penthievre 75008 Paris",
    "description":
        "Pate & Co is a culinary brand established by chef Chang in 2008 with the opening of Pate & Co Cake bar.",
  },
  {
    "image": "assets/restaurant4.jpg",
    "title": "Boxcar Social",
    "star_count": 4,
    "reviews": 21,
    "price": 12,
    "address": "Paris 18e",
    "full_address": "48Penthievre 65008 Paris",
    "description":
        "Boxcar Social is a culinary brand established by chef Smith in 2013 with the opening of Boxcar Social BBQ bar.",
  },
];
