import 'package:email_password_login/emploee/pages/mybookings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Request/userrequest/pages/listpage.dart';
import '../card/homecard.dart';
import '../emploee/pages/listpage.dart';

void main() {
  runApp(const MyAppad());
}

class MyAppad extends StatelessWidget {
  const MyAppad({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Admin Side',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const MyHomePage(title: 'Admin Side'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.request_page),
              title: Text("Request"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Listrequest(),
                    ),
                    (route) => false);
              },
            ),
            ListTile(
              leading: Icon(Icons.people),
              title: Text("Employee"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ListPage(),
                    ),
                    (route) => false);
              },
            ),
            ListTile(
              leading: Icon(Icons.request_page),
              title: Text("My Bookings"),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyBookings(),
                    ),
                    (route) => false);
              },
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
